## Descrição
Api REST que disponibiliza serviços de consulta,listagem,inserção e remoção de dados de planetas. 
A api também consulta a API publica https://swapi.co/ para obter a informção do número de 
aparições em filmes do Star Wars que cada planeta possui.

## Tecnologias utilizadas
Linguagem: Java 8
Banco de dados: MongoDB

## Subindo o serviço
Para subir o serviço será necessário ter o Maven instalado e executar o comdando

mvn clean install -Pdev

Obs.: "dev" é o profile contindo no arquivo pom.xml do projeto contendo as informações de conexão com a base de dados local e path de log.
Caso deseje subir o serviço outros ambientes, novos profiles deverão ser criados e  e outros ambientes deverão ser criados outros profiles com o

## Serviços disponíveis
| Nome                | URI                 |Request Type
| --------------------| :------------------:|:------------------:|
| Listagem de planetas| /planet/list | GET
| Busca de planetas por nome| /planet?name={name} | GET
| Busca de planetas por id| /planet/{id} | GET
| Busca de planetas por id com informação de aparições em filmes Star Wars | /planet/{id}/films | GET
| Adição de planetas| /planet/add | POST
| Remoção de planetas| /planet/remove | DELETE

## Exemplos de chamadas
https://documenter.getpostman.com/view/6734785/SVYow1PB
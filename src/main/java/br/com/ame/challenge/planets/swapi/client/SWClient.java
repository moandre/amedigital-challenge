package br.com.ame.challenge.planets.swapi.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.ame.challenge.planets.service.ExecutionService;
import br.com.ame.challenge.planets.swapi.client.dto.SWPlanet;
import br.com.ame.challenge.planets.swapi.client.dto.SWPlanetList;

@Service
public class SWClient extends ExecutionService{

	@Autowired
	private RestTemplate restTemplate;

	private final String SWAPI_URL = "https://swapi.co/api";

	public SWPlanet searchPlanetById(String id) 
	{
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(SWAPI_URL + "/planets/" + id);

		HttpEntity<?> entity = new HttpEntity<>(headers);

		SWPlanet planet = null;

		ResponseEntity<SWPlanet> response = restTemplate.exchange(
				builder.build().encode().toUri(),
				HttpMethod.GET,
				entity,
				SWPlanet.class);

		getLogger().debug(String.format("searching swapi planet by id=%s - response code %s", id, response.getStatusCodeValue()));
		
		if(HttpStatus.OK.equals(response.getStatusCode()) && response.hasBody()) 
			planet = response.getBody();

		return planet;
	}

	public SWPlanet findByName(String name)
	{
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(SWAPI_URL + "/planets");

		HttpEntity<?> entity = new HttpEntity<>(headers);

		SWPlanet swPlanetResult = null;

		ResponseEntity<SWPlanetList> response = restTemplate.exchange(
				builder.build().encode().toUri(),
				HttpMethod.GET,
				entity,
				SWPlanetList.class);
		
		getLogger().debug(String.format("listing swapi planets page %s - response code %s",1,response.getStatusCodeValue()));

		int page = 2;

		while(response.getBody().getNext()!=null) 
		{
			swPlanetResult = response.getBody().getResults().stream().filter(swPlanet -> swPlanet.getName().toLowerCase().equals(name.toLowerCase())).findFirst().orElse(null);

			if(swPlanetResult!=null)
				return swPlanetResult;

			builder = UriComponentsBuilder.fromHttpUrl(SWAPI_URL + "/planets").queryParam("page", page++); 

			response = restTemplate.exchange(
					builder.build().encode().toUri(),
					HttpMethod.GET,
					entity,
					SWPlanetList.class);	
			
			getLogger().debug(String.format("listing swapi page %s - response code %s",page,response.getStatusCodeValue()));
		}

		return swPlanetResult;
	}

}

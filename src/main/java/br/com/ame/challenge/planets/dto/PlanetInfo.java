package br.com.ame.challenge.planets.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import br.com.ame.challenge.planets.model.Planet;

@JsonInclude(Include.NON_NULL)
public class PlanetInfo {
	
	private String id;
		
	private String name;
	
	private String terrain;
	
	private String climate;
	
	private Integer numberOfFilmAppearances;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTerrain() {
		return terrain;
	}

	public void setTerrain(String terrain) {
		this.terrain = terrain;
	}

	public String getClimate() {
		return climate;
	}

	public void setClimate(String climate) {
		this.climate = climate;
	}

	public Integer getNumberOfFilmAppearances() {
		return numberOfFilmAppearances;
	}

	public void setNumberOfFilmAppearances(Integer numberOfFilmAppearances) {
		this.numberOfFilmAppearances = numberOfFilmAppearances;
	}
	
	public static PlanetInfo prototype(Planet planet) 
	{	
		PlanetInfo planetInfo = new PlanetInfo();
		planetInfo.setId(planet.getId());
		planetInfo.setName(planet.getName());
		planetInfo.setClimate(planet.getClimate());
		planetInfo.setTerrain(planet.getTerrain());
		
		return planetInfo;
	}
}

package br.com.ame.challenge.planets.swapi.client.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SWPlanetList {

	@JsonProperty("count")
	private Integer count;
	
	@JsonProperty("next")
	private String next;
	
	@JsonProperty("previous")
	private Object previous;
	
	@JsonProperty("results")
	private List<SWPlanet> results = new ArrayList<SWPlanet>();

	@JsonProperty("count")
	public Integer getCount() {
		return count;
	}

	@JsonProperty("count")
	public void setCount(Integer count) {
		this.count = count;
	}

	@JsonProperty("next")
	public String getNext() {
		return next;
	}

	@JsonProperty("next")
	public void setNext(String next) {
		this.next = next;
	}

	@JsonProperty("previous")
	public Object getPrevious() {
		return previous;
	}

	@JsonProperty("previous")
	public void setPrevious(Object previous) {
		this.previous = previous;
	}

	@JsonProperty("results")
	public List<SWPlanet> getResults() {
		return results;
	}

	@JsonProperty("results")
	public void setResults(List<SWPlanet> results) {
		this.results = results;
	}
}
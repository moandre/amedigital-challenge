package br.com.ame.challenge.planets.dto;

public class ServiceError {
	
	private String message;

	public ServiceError(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}

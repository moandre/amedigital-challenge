package br.com.ame.challenge.planets.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.ame.challenge.planets.model.Planet;

public interface PlanetRepository extends MongoRepository<Planet, String> {
		
	public List<Planet> findByName(String name);
		
}

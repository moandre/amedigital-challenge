package br.com.ame.challenge.planets.service;

import org.apache.logging.log4j.LogManager;
import org.springframework.stereotype.Service;

@Service
public class ExecutionService extends AbstractExecutionService {

    public ExecutionService() {
        setLogger(LogManager.getLogger(this.getClass()));
    }

    @Override
    public void startApplication() {
        getLogger().info("Application started");
    }

    @Override
    public void stopApplication() {
        getLogger().info("Application stopped");
    }
}

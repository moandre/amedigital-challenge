package br.com.ame.challenge.planets.service.exception;

public class ServiceException extends Exception{
	
	private static final long serialVersionUID = 3066802307225243931L;
	private String message;

	public ServiceException(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}

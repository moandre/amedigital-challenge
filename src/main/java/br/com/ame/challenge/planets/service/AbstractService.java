package br.com.ame.challenge.planets.service;

import org.apache.logging.log4j.Logger;

public abstract class AbstractService {

    private Logger logger;
            
    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }
}

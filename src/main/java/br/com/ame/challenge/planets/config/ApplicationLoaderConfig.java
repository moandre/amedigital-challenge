package br.com.ame.challenge.planets.config;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.convert.DbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

@Configuration
@EnableMongoRepositories(basePackages="br.com.ame.challenge.planets.repository")
@EnableMongoAuditing
public class ApplicationLoaderConfig {	
	
	public Logger logger = LogManager.getLogger(this.getClass());
	
	@Bean
	public PropertyPlaceholderConfigurer properties() {

		final PropertyPlaceholderConfigurer ppc = new PropertyPlaceholderConfigurer();
		ppc.setIgnoreResourceNotFound(true);

		final List<Resource> resourceLst = new ArrayList<Resource>();

		resourceLst.add(new ClassPathResource("application.properties"));
		resourceLst.add(new ClassPathResource("config/connection.properties"));
		resourceLst.add(new ClassPathResource("dev/config/connection.properties"));
		resourceLst.add(new ClassPathResource("dev/application.properties"));

		ppc.setLocations(resourceLst.toArray(new Resource[]{}));

		return ppc;
	}

	@Bean
	public RestTemplate restTemplate()
	{

		RequestConfig requestConfig = RequestConfig.custom()
				.setConnectionRequestTimeout(30000)
				.setConnectTimeout(30000)
				.setSocketTimeout(30000)
				.build();

		PoolingHttpClientConnectionManager poolingHttpClientConnectionManager = new PoolingHttpClientConnectionManager();
		poolingHttpClientConnectionManager.setMaxTotal(50);

		CloseableHttpClient httpClientBuilder = HttpClientBuilder.create()
				.setConnectionManager(poolingHttpClientConnectionManager)
				.setDefaultRequestConfig(requestConfig)
				.build();

		HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
		httpRequestFactory.setHttpClient(httpClientBuilder);

		return new RestTemplate(httpRequestFactory);
	}
	
	@Bean
	public MongoClient mongo(@Value("${mongo.uri}") String uri,@Value("${mongo.user}") String username, @Value("${mongo.password}") String password, @Value("${mongo.database}") String database) throws Exception 
	{		
		return  new MongoClient(new MongoClientURI(String.format("mongodb://%s:%s@%s/%s", username,password,uri,database)));
	}

	@Bean
	public MongoDbFactory mongoDbFactory(@Value("${mongo.uri}") String uri, @Value("${mongo.user}") String username, @Value("${mongo.password}") String password, @Value("${mongo.database}") String database) throws Exception 
	{
		return new SimpleMongoDbFactory(mongo(uri,username,password,database),database);
	}

	@Bean
	public MongoTemplate mongoTemplate(@Value("${mongo.uri}") String uri, @Value("${mongo.user}") String username, @Value("${mongo.password}") String password, @Value("${mongo.database}") String database) throws Exception 
	{		
		MongoDbFactory mongoDbFactory = mongoDbFactory(uri,username,password,database);
		DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoDbFactory);
		MappingMongoConverter converter = new MappingMongoConverter(dbRefResolver, new MongoMappingContext());
		converter.setTypeMapper(new DefaultMongoTypeMapper(null));
		return new MongoTemplate(mongoDbFactory, converter);
	}
}

package br.com.ame.challenge.planets.model;

import org.springframework.data.annotation.Id;

import br.com.ame.challenge.planets.dto.PlanetInfo;

public class Planet {
	
    @Id
    private String id;
    	
	private String name;
	
	private String terrain;
	
	private String climate;

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTerrain() {
		return terrain;
	}

	public void setTerrain(String terrain) {
		this.terrain = terrain;
	}

	public String getClimate() {
		return climate;
	}

	public void setClimate(String climate) {
		this.climate = climate;
	}
	
	public static Planet prototype(PlanetInfo planetInfo) 
	{	
		Planet planet = new Planet();
		planet.setName(planetInfo.getName());
		planet.setClimate(planetInfo.getClimate());
		planet.setTerrain(planetInfo.getTerrain());
		
		return planet;
	}
	
}

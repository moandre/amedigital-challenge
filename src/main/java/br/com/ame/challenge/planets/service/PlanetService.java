package br.com.ame.challenge.planets.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ame.challenge.planets.dto.PlanetInfo;
import br.com.ame.challenge.planets.model.Planet;
import br.com.ame.challenge.planets.repository.PlanetRepository;
import br.com.ame.challenge.planets.service.exception.ServiceException;
import br.com.ame.challenge.planets.service.exception.ServiceExceptionMessage;
import br.com.ame.challenge.planets.swapi.client.SWClient;
import br.com.ame.challenge.planets.swapi.client.dto.SWPlanet;

@Service
public class PlanetService extends ExecutionService{
	
	@Autowired
	private SWClient swClient;

	@Autowired
	private PlanetRepository planetRepository;

	public PlanetInfo searchPlanetById(String id) throws Exception
	{
		getLogger().debug(String.format("searching planet by id=%s", id));
		
		Planet planet = planetRepository.findById(id).orElse(null);

		if(planet!=null)
			return PlanetInfo.prototype(planet);		
		else
			throw new ServiceException(ServiceExceptionMessage.PLANET_NOT_FOUND);
	}

	public List<PlanetInfo> searchPlanetByName(String name) throws Exception
	{
		getLogger().debug(String.format("searching planet by name=%s", name));
		
		List<Planet> planets = planetRepository.findByName(name);

		final List<PlanetInfo> planetInfos = new ArrayList<PlanetInfo>();

		planets.stream().forEach(planet -> {
			PlanetInfo planetInfo = PlanetInfo.prototype(planet);	
			planetInfos.add(planetInfo);
		});	
		
		return planetInfos;
	}

	public PlanetInfo searchPlanetWithFilmInfo(String id) throws Exception
	{
		getLogger().debug(String.format("searching planet full information by id=%s", id));
		
		Planet planet = planetRepository.findById(id).orElse(null);

		if(planet!=null) 
		{
			PlanetInfo planetInfo = PlanetInfo.prototype(planet);
			planetInfo.setNumberOfFilmAppearances(getNumberOfFilmAppearances(planetInfo.getName()));
			return planetInfo;
		}
		else
			throw new ServiceException(ServiceExceptionMessage.PLANET_NOT_FOUND);
	}

	public List<PlanetInfo> listAll() throws Exception 
	{
		getLogger().debug(String.format("listing planets"));
		
		List<Planet> planets = planetRepository.findAll();

		final List<PlanetInfo> planetInfos = new ArrayList<PlanetInfo>();

		if(planets!=null)
		{
			planets.stream().forEach(planet -> {
				PlanetInfo planetInfo = PlanetInfo.prototype(planet);	
				planetInfos.add(planetInfo);
			});	
		}

		return planetInfos;
	}

	public Integer getNumberOfFilmAppearances(String planetName) throws Exception
	{
		getLogger().debug(String.format("calling swapi"));

		SWPlanet swPlanet = swClient.findByName(planetName);
		
		if (swPlanet !=null)  
			return swPlanet.getFilms().size();
		return 0;
	}

	public PlanetInfo add(PlanetInfo planetInfo) throws Exception 
	{
		getLogger().debug(String.format("saving planet %s ", planetInfo.getName()));
		
		Planet planet = Planet.prototype(planetInfo);

		planet = planetRepository.save(planet);
		
		getLogger().debug(String.format("planet %s successfully saved", planetInfo.getName()));

		planetInfo = PlanetInfo.prototype(planet);

		return planetInfo;
	}

	public void remove(PlanetInfo planetInfo) throws ServiceException 
	{
		getLogger().debug(String.format("deleting planet %s ", planetInfo.getName()));
		
		Planet planet = planetRepository.findById(planetInfo.getId()).orElse(null);

		if(planet!=null) 
		{
			planetRepository.delete(planet);
			getLogger().debug(String.format("planet %s successfully deleted", planetInfo.getName()));
		}
		else	
			throw new ServiceException(ServiceExceptionMessage.PLANET_NOT_FOUND);
	}
}

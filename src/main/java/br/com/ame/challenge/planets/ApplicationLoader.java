package br.com.ame.challenge.planets;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.com.ame.challenge.planets.service.ExecutionService;

@SpringBootApplication
public class ApplicationLoader {

	@Autowired
	ExecutionService executionService;

	public static void main(String[] args) {
		SpringApplication.run(ApplicationLoader.class, args);
	}

	@PostConstruct
	public void init() {

		executionService.startApplication();
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				executionService.stopApplication();
			}
		});

	}
}

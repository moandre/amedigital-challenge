package br.com.ame.challenge.planets.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.ame.challenge.planets.dto.PlanetInfo;
import br.com.ame.challenge.planets.dto.ServiceError;
import br.com.ame.challenge.planets.service.PlanetService;
import br.com.ame.challenge.planets.service.exception.ServiceException;

@RestController
@RequestMapping("/challenge")
public class PlanetController {
	
	@Autowired
	private PlanetService planetService;

	@GetMapping("/planet/{id}")
	public ResponseEntity<?> getPlanetById(@PathVariable String id)
	{	
		try {
			return new ResponseEntity<PlanetInfo>(planetService.searchPlanetById(id), HttpStatus.OK);
		} catch (ServiceException e) {
			return new ResponseEntity<ServiceError>(new ServiceError(e.getMessage()), HttpStatus.NOT_FOUND);		
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);		
		}
	}  
	
	@GetMapping("/planet/{id}/films")
	public ResponseEntity<?> getPlanetFilmsInfo(@PathVariable String id)
	{	
		try {
			return new ResponseEntity<PlanetInfo>(planetService.searchPlanetWithFilmInfo(id), HttpStatus.OK);
		} catch (ServiceException e) {
			return new ResponseEntity<ServiceError>(new ServiceError(e.getMessage()), HttpStatus.NOT_FOUND);		
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);		
		}
	}  
	
	@GetMapping("/planet")
	public ResponseEntity<?> getPlanetByName(@RequestParam String name)
	{	
		try {
			return new ResponseEntity<List<PlanetInfo>>(planetService.searchPlanetByName(name), HttpStatus.OK);
		} catch (ServiceException e) {
			return new ResponseEntity<ServiceError>(new ServiceError(e.getMessage()), HttpStatus.NOT_FOUND);		
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);		
		}
	} 

	@GetMapping("/planet/list")
	public ResponseEntity<List<PlanetInfo>> listPlanets()
	{	
		try {
			return new ResponseEntity<List<PlanetInfo>>(planetService.listAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);		
		}
	} 

	@PostMapping(path="/planet/add")
	public ResponseEntity<PlanetInfo> addPlanet(@RequestBody PlanetInfo newPlanet)
	{	
		try {
			return new ResponseEntity<PlanetInfo>(planetService.add(newPlanet), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);		
		}
	} 

	@DeleteMapping("/planet/remove")
	public ResponseEntity<?> removePlanet(@RequestBody PlanetInfo planet)
	{	
		try {
			planetService.remove(planet);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (ServiceException e) {
			return new ResponseEntity<ServiceError>(new ServiceError(e.getMessage()), HttpStatus.NOT_FOUND);		
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);		
		}
	} 
}

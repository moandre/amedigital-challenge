package br.com.ame.challenge.planets.service;

public abstract class AbstractExecutionService extends AbstractService {

    public abstract void startApplication();

    public abstract void stopApplication();

}
